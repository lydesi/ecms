import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { TestingDTO } from '../src/service/dto/testing.dto';
import { TestingService } from '../src/service/testing.service';

describe('Testing Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(TestingService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all testings ', async () => {
        const getEntities: TestingDTO[] = (await request(app.getHttpServer()).get('/api/testings').expect(200)).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET testings by id', async () => {
        const getEntity: TestingDTO = (
            await request(app.getHttpServer())
                .get('/api/testings/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create testings', async () => {
        const createdEntity: TestingDTO = (
            await request(app.getHttpServer()).post('/api/testings').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update testings', async () => {
        const updatedEntity: TestingDTO = (
            await request(app.getHttpServer()).put('/api/testings').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update testings from id', async () => {
        const updatedEntity: TestingDTO = (
            await request(app.getHttpServer())
                .put('/api/testings/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE testings', async () => {
        const deletedEntity: TestingDTO = (
            await request(app.getHttpServer())
                .delete('/api/testings/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
