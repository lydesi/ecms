import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { UploadDTO } from '../src/service/dto/upload.dto';
import { UploadService } from '../src/service/upload.service';

describe('Upload Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(UploadService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all uploads ', async () => {
        const getEntities: UploadDTO[] = (await request(app.getHttpServer()).get('/api/uploads').expect(200)).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET uploads by id', async () => {
        const getEntity: UploadDTO = (
            await request(app.getHttpServer())
                .get('/api/uploads/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create uploads', async () => {
        const createdEntity: UploadDTO = (
            await request(app.getHttpServer()).post('/api/uploads').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update uploads', async () => {
        const updatedEntity: UploadDTO = (
            await request(app.getHttpServer()).put('/api/uploads').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update uploads from id', async () => {
        const updatedEntity: UploadDTO = (
            await request(app.getHttpServer())
                .put('/api/uploads/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE uploads', async () => {
        const deletedEntity: UploadDTO = (
            await request(app.getHttpServer())
                .delete('/api/uploads/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
