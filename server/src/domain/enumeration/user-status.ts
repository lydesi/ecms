/**
 * The UserStatus enumeration.
 */
export enum UserStatus {
    TEACHER = 'TEACHER',
    STUDENT = 'STUDENT',
}
