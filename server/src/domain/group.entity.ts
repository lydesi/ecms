/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Upload } from './upload.entity';
import { Subject } from './subject.entity';

/**
 * A Group.
 */
@Entity('jhi_group')
export class Group extends BaseEntity {
    @Column({ name: 'name', nullable: true })
    name: string;

    @Column({ name: 'description', nullable: true })
    description: string;

    @Column({ type: 'blob', name: 'logo', nullable: true })
    logo: any;

    @Column({ name: 'logo_content_type', nullable: true })
    logoContentType: string;

    @ManyToMany((type) => Upload)
    @JoinTable({
        name: 'rel_jhi_group__uploads',
        joinColumn: { name: 'group_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'uploads_id', referencedColumnName: 'id' },
    })
    uploads: Upload[];

    @ManyToMany((type) => Subject)
    @JoinTable({
        name: 'rel_jhi_group__subjects',
        joinColumn: { name: 'group_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'subjects_id', referencedColumnName: 'id' },
    })
    subjects: Subject[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
