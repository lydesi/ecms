/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Subject } from './subject.entity';
import { Upload } from './upload.entity';

/**
 * A Post.
 */
@Entity('post')
export class Post extends BaseEntity {
    @Column({ name: 'title' })
    title: string;

    @Column({ name: 'description', nullable: true })
    description: string;

    @Column({ type: 'boolean', name: 'visible', nullable: true })
    visible: boolean;

    @Column({ type: 'blob', name: 'logo', nullable: true })
    logo: any;

    @Column({ name: 'logo_content_type', nullable: true })
    logoContentType: string;

    @ManyToOne((type) => Subject)
    subject: Subject;

    @ManyToMany((type) => Upload)
    @JoinTable({
        name: 'rel_post__uploads',
        joinColumn: { name: 'post_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'uploads_id', referencedColumnName: 'id' },
    })
    uploads: Upload[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
