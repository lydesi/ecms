/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Group } from './group.entity';
import { UserStatus } from './enumeration/user-status';

import { User } from './user.entity';

/**
 * A Profile.
 */
@Entity('profile')
export class Profile extends BaseEntity {
    @Column({ type: 'simple-enum', name: 'status', enum: UserStatus })
    status: UserStatus;

    @Column({ type: 'blob', name: 'avatar', nullable: true })
    avatar: any;

    @Column({ name: 'avatar_content_type', nullable: true })
    avatarContentType: string;

    @ManyToOne((type) => Group)
    group: Group;

    @ManyToOne((type) => User)
    user: User;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
