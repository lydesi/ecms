/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Upload } from './upload.entity';
import { Group } from './group.entity';

/**
 * A Subject.
 */
@Entity('subject')
export class Subject extends BaseEntity {
    @Column({ name: 'name' })
    name: string;

    @Column({ name: 'description', nullable: true })
    description: string;

    @Column({ type: 'blob', name: 'logo', nullable: true })
    logo: any;

    @Column({ name: 'logo_content_type', nullable: true })
    logoContentType: string;

    @ManyToMany((type) => Upload)
    @JoinTable({
        name: 'rel_subject__uploads',
        joinColumn: { name: 'subject_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'uploads_id', referencedColumnName: 'id' },
    })
    uploads: Upload[];

    @ManyToMany((type) => Group)
    groups: Group[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
