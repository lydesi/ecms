/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Subject } from './subject.entity';
import { Upload } from './upload.entity';

/**
 * A Testing.
 */
@Entity('testing')
export class Testing extends BaseEntity {
    @Column({ name: 'title' })
    title: string;

    @Column({ name: 'description', nullable: true })
    description: string;

    @Column({ name: 'url' })
    url: string;

    @ManyToOne((type) => Subject)
    subject: Subject;

    @ManyToMany((type) => Upload)
    @JoinTable({
        name: 'rel_testing__uploads',
        joinColumn: { name: 'testing_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'uploads_id', referencedColumnName: 'id' },
    })
    uploads: Upload[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
