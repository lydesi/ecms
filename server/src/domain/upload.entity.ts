/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Post } from './post.entity';
import { Subject } from './subject.entity';
import { Group } from './group.entity';
import { Testing } from './testing.entity';

/**
 * A Upload.
 */
@Entity('upload')
export class Upload extends BaseEntity {
    @Column({ name: 'filename', nullable: true })
    filename: string;

    @Column({ type: 'blob', name: 'data', nullable: true })
    data: any;

    @Column({ name: 'data_content_type', nullable: true })
    dataContentType: string;

    @ManyToMany((type) => Post)
    posts: Post[];

    @ManyToMany((type) => Subject)
    subjects: Subject[];

    @ManyToMany((type) => Group)
    groups: Group[];

    @ManyToMany((type) => Testing)
    testings: Testing[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
