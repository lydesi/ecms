import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TestingController } from '../web/rest/testing.controller';
import { TestingRepository } from '../repository/testing.repository';
import { TestingService } from '../service/testing.service';

@Module({
    imports: [TypeOrmModule.forFeature([TestingRepository])],
    controllers: [TestingController],
    providers: [TestingService],
    exports: [TestingService],
})
export class TestingModule {}
