import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UploadController } from '../web/rest/upload.controller';
import { UploadRepository } from '../repository/upload.repository';
import { UploadService } from '../service/upload.service';

@Module({
    imports: [TypeOrmModule.forFeature([UploadRepository])],
    controllers: [UploadController],
    providers: [UploadService],
    exports: [UploadService],
})
export class UploadModule {}
