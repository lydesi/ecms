import { EntityRepository, Repository } from 'typeorm';
import { Testing } from '../domain/testing.entity';

@EntityRepository(Testing)
export class TestingRepository extends Repository<Testing> {}
