import { EntityRepository, Repository } from 'typeorm';
import { Upload } from '../domain/upload.entity';

@EntityRepository(Upload)
export class UploadRepository extends Repository<Upload> {}
