/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { UploadDTO } from './upload.dto';
import { SubjectDTO } from './subject.dto';

/**
 * A GroupDTO object.
 */
export class GroupDTO extends BaseDTO {
    @ApiModelProperty({ description: 'name field', required: false })
    name: string;

    @ApiModelProperty({ description: 'description field', required: false })
    description: string;

    @ApiModelProperty({ description: 'logo field', required: false })
    logo: any;

    logoContentType: string;

    @ApiModelProperty({ type: UploadDTO, isArray: true, description: 'uploads relationship' })
    uploads: UploadDTO[];

    @ApiModelProperty({ type: SubjectDTO, isArray: true, description: 'subjects relationship' })
    subjects: SubjectDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
