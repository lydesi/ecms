/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { SubjectDTO } from './subject.dto';
import { UploadDTO } from './upload.dto';

/**
 * A PostDTO object.
 */
export class PostDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'title field' })
    title: string;

    @ApiModelProperty({ description: 'description field', required: false })
    description: string;

    @ApiModelProperty({ description: 'visible field', required: false })
    visible: boolean;

    @ApiModelProperty({ description: 'logo field', required: false })
    logo: any;

    logoContentType: string;

    @ApiModelProperty({ type: SubjectDTO, description: 'subject relationship' })
    subject: SubjectDTO;

    @ApiModelProperty({ type: UploadDTO, isArray: true, description: 'uploads relationship' })
    uploads: UploadDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
