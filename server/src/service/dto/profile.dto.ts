/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { GroupDTO } from './group.dto';
import { UserStatus } from '../../domain/enumeration/user-status';

import { UserDTO } from './user.dto';

/**
 * A ProfileDTO object.
 */
export class ProfileDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ enum: UserStatus, description: 'status enum field' })
    status: UserStatus;

    @ApiModelProperty({ description: 'avatar field', required: false })
    avatar: any;

    avatarContentType: string;

    @ApiModelProperty({ type: GroupDTO, description: 'group relationship' })
    group: GroupDTO;

    @ApiModelProperty({ type: UserDTO, description: 'user relationship' })
    user: UserDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
