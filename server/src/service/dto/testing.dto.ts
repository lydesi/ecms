/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { SubjectDTO } from './subject.dto';
import { UploadDTO } from './upload.dto';

/**
 * A TestingDTO object.
 */
export class TestingDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'title field' })
    title: string;

    @ApiModelProperty({ description: 'description field', required: false })
    description: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'url field' })
    url: string;

    @ApiModelProperty({ type: SubjectDTO, description: 'subject relationship' })
    subject: SubjectDTO;

    @ApiModelProperty({ type: UploadDTO, isArray: true, description: 'uploads relationship' })
    uploads: UploadDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
