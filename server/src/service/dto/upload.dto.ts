/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { PostDTO } from './post.dto';
import { SubjectDTO } from './subject.dto';
import { GroupDTO } from './group.dto';
import { TestingDTO } from './testing.dto';

/**
 * A UploadDTO object.
 */
export class UploadDTO extends BaseDTO {
    @ApiModelProperty({ description: 'filename field', required: false })
    filename: string;

    @ApiModelProperty({ description: 'data field', required: false })
    data: any;

    dataContentType: string;

    @ApiModelProperty({ type: PostDTO, isArray: true, description: 'posts relationship' })
    posts: PostDTO[];

    @ApiModelProperty({ type: SubjectDTO, isArray: true, description: 'subjects relationship' })
    subjects: SubjectDTO[];

    @ApiModelProperty({ type: GroupDTO, isArray: true, description: 'groups relationship' })
    groups: GroupDTO[];

    @ApiModelProperty({ type: TestingDTO, isArray: true, description: 'testings relationship' })
    testings: TestingDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
