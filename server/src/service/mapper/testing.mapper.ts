import { Testing } from '../../domain/testing.entity';
import { TestingDTO } from '../dto/testing.dto';

/**
 * A Testing mapper object.
 */
export class TestingMapper {
    static fromDTOtoEntity(entityDTO: TestingDTO): Testing {
        if (!entityDTO) {
            return;
        }
        let entity = new Testing();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Testing): TestingDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new TestingDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
