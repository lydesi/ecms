import { Upload } from '../../domain/upload.entity';
import { UploadDTO } from '../dto/upload.dto';

/**
 * A Upload mapper object.
 */
export class UploadMapper {
    static fromDTOtoEntity(entityDTO: UploadDTO): Upload {
        if (!entityDTO) {
            return;
        }
        let entity = new Upload();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Upload): UploadDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new UploadDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
