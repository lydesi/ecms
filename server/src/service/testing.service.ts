import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { TestingDTO } from '../service/dto/testing.dto';
import { TestingMapper } from '../service/mapper/testing.mapper';
import { TestingRepository } from '../repository/testing.repository';

const relationshipNames = [];
relationshipNames.push('subject');
relationshipNames.push('uploads');

@Injectable()
export class TestingService {
    logger = new Logger('TestingService');

    constructor(@InjectRepository(TestingRepository) private testingRepository: TestingRepository) {}

    async findById(id: number): Promise<TestingDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.testingRepository.findOne(id, options);
        return TestingMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<TestingDTO>): Promise<TestingDTO | undefined> {
        const result = await this.testingRepository.findOne(options);
        return TestingMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<TestingDTO>): Promise<[TestingDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.testingRepository.findAndCount(options);
        const testingDTO: TestingDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach((testing) => testingDTO.push(TestingMapper.fromEntityToDTO(testing)));
            resultList[0] = testingDTO;
        }
        return resultList;
    }

    async save(testingDTO: TestingDTO, creator?: string): Promise<TestingDTO | undefined> {
        const entity = TestingMapper.fromDTOtoEntity(testingDTO);
        if (creator) {
            if (!entity.createdBy) {
                entity.createdBy = creator;
            }
            entity.lastModifiedBy = creator;
        }
        const result = await this.testingRepository.save(entity);
        return TestingMapper.fromEntityToDTO(result);
    }

    async update(testingDTO: TestingDTO, updater?: string): Promise<TestingDTO | undefined> {
        const entity = TestingMapper.fromDTOtoEntity(testingDTO);
        if (updater) {
            entity.lastModifiedBy = updater;
        }
        const result = await this.testingRepository.save(entity);
        return TestingMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<void | undefined> {
        await this.testingRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
