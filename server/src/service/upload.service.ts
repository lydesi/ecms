import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { UploadDTO } from '../service/dto/upload.dto';
import { UploadMapper } from '../service/mapper/upload.mapper';
import { UploadRepository } from '../repository/upload.repository';

const relationshipNames = [];

@Injectable()
export class UploadService {
    logger = new Logger('UploadService');

    constructor(@InjectRepository(UploadRepository) private uploadRepository: UploadRepository) {}

    async findById(id: number): Promise<UploadDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.uploadRepository.findOne(id, options);
        return UploadMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<UploadDTO>): Promise<UploadDTO | undefined> {
        const result = await this.uploadRepository.findOne(options);
        return UploadMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<UploadDTO>): Promise<[UploadDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.uploadRepository.findAndCount(options);
        const uploadDTO: UploadDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach((upload) => uploadDTO.push(UploadMapper.fromEntityToDTO(upload)));
            resultList[0] = uploadDTO;
        }
        return resultList;
    }

    async save(uploadDTO: UploadDTO, creator?: string): Promise<UploadDTO | undefined> {
        const entity = UploadMapper.fromDTOtoEntity(uploadDTO);
        if (creator) {
            if (!entity.createdBy) {
                entity.createdBy = creator;
            }
            entity.lastModifiedBy = creator;
        }
        const result = await this.uploadRepository.save(entity);
        return UploadMapper.fromEntityToDTO(result);
    }

    async update(uploadDTO: UploadDTO, updater?: string): Promise<UploadDTO | undefined> {
        const entity = UploadMapper.fromDTOtoEntity(uploadDTO);
        if (updater) {
            entity.lastModifiedBy = updater;
        }
        const result = await this.uploadRepository.save(entity);
        return UploadMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<void | undefined> {
        await this.uploadRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
