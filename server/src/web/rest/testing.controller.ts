import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { TestingDTO } from '../../service/dto/testing.dto';
import { TestingService } from '../../service/testing.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/testings')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('testings')
export class TestingController {
    logger = new Logger('TestingController');

    constructor(private readonly testingService: TestingService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: TestingDTO,
    })
    async getAll(@Req() req: Request): Promise<TestingDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);

      const subjectId = req.query['subjectId.equals']

      if(subjectId) {
        const [results, count] = await this.testingService.findAndCount({
          where: {
            subject: {
              id: subjectId
            }
          }
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
      }

        const [results, count] = await this.testingService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TestingDTO,
    })
    async getOne(@Param('id') id: number): Promise<TestingDTO> {
        return await this.testingService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create testing' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: TestingDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() testingDTO: TestingDTO): Promise<TestingDTO> {
        const created = await this.testingService.save(testingDTO, req.user?.login);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Testing', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update testing' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TestingDTO,
    })
    async put(@Req() req: Request, @Body() testingDTO: TestingDTO): Promise<TestingDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Testing', testingDTO.id);
        return await this.testingService.update(testingDTO, req.user?.login);
    }

    @Put('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update testing with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TestingDTO,
    })
    async putId(@Req() req: Request, @Body() testingDTO: TestingDTO): Promise<TestingDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Testing', testingDTO.id);
        return await this.testingService.update(testingDTO, req.user?.login);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete testing' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Testing', id);
        return await this.testingService.deleteById(id);
    }
}
