import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { UploadDTO } from '../../service/dto/upload.dto';
import { UploadService } from '../../service/upload.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/uploads')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('uploads')
export class UploadController {
    logger = new Logger('UploadController');

    constructor(private readonly uploadService: UploadService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: UploadDTO,
    })
    async getAll(@Req() req: Request): Promise<UploadDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.uploadService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: UploadDTO,
    })
    async getOne(@Param('id') id: number): Promise<UploadDTO> {
        return await this.uploadService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create upload' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: UploadDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() uploadDTO: UploadDTO): Promise<UploadDTO> {
        const created = await this.uploadService.save(uploadDTO, req.user?.login);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Upload', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update upload' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: UploadDTO,
    })
    async put(@Req() req: Request, @Body() uploadDTO: UploadDTO): Promise<UploadDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Upload', uploadDTO.id);
        return await this.uploadService.update(uploadDTO, req.user?.login);
    }

    @Put('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update upload with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: UploadDTO,
    })
    async putId(@Req() req: Request, @Body() uploadDTO: UploadDTO): Promise<UploadDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Upload', uploadDTO.id);
        return await this.uploadService.update(uploadDTO, req.user?.login);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete upload' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Upload', id);
        return await this.uploadService.deleteById(id);
    }
}
