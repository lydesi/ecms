import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import UploadService from '@/entities/upload/upload.service';
import { IUpload } from '@/shared/model/upload.model';

import SubjectService from '@/entities/subject/subject.service';
import { ISubject } from '@/shared/model/subject.model';

import { IGroup, Group } from '@/shared/model/group.model';
import GroupService from './group.service';

const validations: any = {
  group: {
    name: {},
    description: {},
    logo: {},
  },
};

@Component({
  validations,
})
export default class GroupUpdate extends mixins(JhiDataUtils) {
  @Inject('groupService') private groupService: () => GroupService;
  public group: IGroup = new Group();

  @Inject('uploadService') private uploadService: () => UploadService;

  public uploads: IUpload[] = [];

  @Inject('subjectService') private subjectService: () => SubjectService;

  public subjects: ISubject[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.groupId) {
        vm.retrieveGroup(to.params.groupId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.group.uploads = [];
    this.group.subjects = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.group.id) {
      this.groupService()
        .update(this.group)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.group.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.groupService()
        .create(this.group)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.group.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveGroup(groupId): void {
    this.groupService()
      .find(groupId)
      .then(res => {
        this.group = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public clearInputImage(field, fieldContentType, idInput): void {
    if (this.group && field && fieldContentType) {
      if (Object.prototype.hasOwnProperty.call(this.group, field)) {
        this.group[field] = null;
      }
      if (Object.prototype.hasOwnProperty.call(this.group, fieldContentType)) {
        this.group[fieldContentType] = null;
      }
      if (idInput) {
        (<any>this).$refs[idInput] = null;
      }
    }
  }

  public initRelationships(): void {
    this.uploadService()
      .retrieve()
      .then(res => {
        this.uploads = res.data;
      });
    this.subjectService()
      .retrieve()
      .then(res => {
        this.subjects = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
