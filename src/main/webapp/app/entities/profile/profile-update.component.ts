import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';

import GroupService from '@/entities/group/group.service';
import { IGroup } from '@/shared/model/group.model';

import UserService from '@/admin/user-management/user-management.service';

import { IProfile, Profile } from '@/shared/model/profile.model';
import ProfileService from './profile.service';

const validations: any = {
  profile: {
    status: {
      required,
    },
    avatar: {},
  },
};

@Component({
  validations,
})
export default class ProfileUpdate extends mixins(JhiDataUtils) {
  @Inject('profileService') private profileService: () => ProfileService;
  public profile: IProfile = new Profile();

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];

  @Inject('userService') private userService: () => UserService;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.profileId) {
        vm.retrieveProfile(to.params.profileId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.profile.id) {
      this.profileService()
        .update(this.profile)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.profile.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.profileService()
        .create(this.profile)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.profile.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveProfile(profileId): void {
    this.profileService()
      .find(profileId)
      .then(res => {
        this.profile = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public clearInputImage(field, fieldContentType, idInput): void {
    if (this.profile && field && fieldContentType) {
      if (Object.prototype.hasOwnProperty.call(this.profile, field)) {
        this.profile[field] = null;
      }
      if (Object.prototype.hasOwnProperty.call(this.profile, fieldContentType)) {
        this.profile[fieldContentType] = null;
      }
      if (idInput) {
        (<any>this).$refs[idInput] = null;
      }
    }
  }

  public initRelationships(): void {
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
    this.userService()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
