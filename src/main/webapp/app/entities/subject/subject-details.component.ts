import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ISubject } from '@/shared/model/subject.model';
import SubjectService from './subject.service';

@Component
export default class SubjectDetails extends mixins(JhiDataUtils) {
  @Inject('subjectService') private subjectService: () => SubjectService;
  public subject: ISubject = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.subjectId) {
        vm.retrieveSubject(to.params.subjectId);
      }
    });
  }

  public retrieveSubject(subjectId) {
    this.subjectService()
      .find(subjectId)
      .then(res => {
        this.subject = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
