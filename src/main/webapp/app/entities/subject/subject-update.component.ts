import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';

import UploadService from '@/entities/upload/upload.service';
import { IUpload } from '@/shared/model/upload.model';

import GroupService from '@/entities/group/group.service';
import { IGroup } from '@/shared/model/group.model';

import { ISubject, Subject } from '@/shared/model/subject.model';
import SubjectService from './subject.service';

const validations: any = {
  subject: {
    name: {
      required,
    },
    description: {},
    logo: {},
  },
};

@Component({
  validations,
})
export default class SubjectUpdate extends mixins(JhiDataUtils) {
  @Inject('subjectService') private subjectService: () => SubjectService;
  public subject: ISubject = new Subject();

  @Inject('uploadService') private uploadService: () => UploadService;

  public uploads: IUpload[] = [];

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.subjectId) {
        vm.retrieveSubject(to.params.subjectId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.subject.uploads = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.subject.id) {
      this.subjectService()
        .update(this.subject)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.subject.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.subjectService()
        .create(this.subject)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.subject.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveSubject(subjectId): void {
    this.subjectService()
      .find(subjectId)
      .then(res => {
        this.subject = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public clearInputImage(field, fieldContentType, idInput): void {
    if (this.subject && field && fieldContentType) {
      if (Object.prototype.hasOwnProperty.call(this.subject, field)) {
        this.subject[field] = null;
      }
      if (Object.prototype.hasOwnProperty.call(this.subject, fieldContentType)) {
        this.subject[fieldContentType] = null;
      }
      if (idInput) {
        (<any>this).$refs[idInput] = null;
      }
    }
  }

  public initRelationships(): void {
    this.uploadService()
      .retrieve()
      .then(res => {
        this.uploads = res.data;
      });
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
