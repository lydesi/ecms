import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ISubject } from '@/shared/model/subject.model';

import JhiDataUtils from '@/shared/data/data-utils.service';

import SubjectService from './subject.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Subject extends mixins(JhiDataUtils) {
  @Inject('subjectService') private subjectService: () => SubjectService;
  private removeId: number = null;

  public subjects: ISubject[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllSubjects();
  }

  public clear(): void {
    this.retrieveAllSubjects();
  }

  public retrieveAllSubjects(): void {
    this.isFetching = true;

    this.subjectService()
      .retrieve()
      .then(
        res => {
          this.subjects = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: ISubject): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeSubject(): void {
    this.subjectService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecmsApp.subject.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllSubjects();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
