import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITesting } from '@/shared/model/testing.model';
import TestingService from './testing.service';

@Component
export default class TestingDetails extends Vue {
  @Inject('testingService') private testingService: () => TestingService;
  public testing: ITesting = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.testingId) {
        vm.retrieveTesting(to.params.testingId);
      }
    });
  }

  public retrieveTesting(testingId) {
    this.testingService()
      .find(testingId)
      .then(res => {
        this.testing = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
