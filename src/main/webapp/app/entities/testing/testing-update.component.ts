import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';

import SubjectService from '@/entities/subject/subject.service';
import { ISubject } from '@/shared/model/subject.model';

import UploadService from '@/entities/upload/upload.service';
import { IUpload } from '@/shared/model/upload.model';

import { ITesting, Testing } from '@/shared/model/testing.model';
import TestingService from './testing.service';

const validations: any = {
  testing: {
    title: {
      required,
    },
    description: {},
    url: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class TestingUpdate extends Vue {
  @Inject('testingService') private testingService: () => TestingService;
  public testing: ITesting = new Testing();

  @Inject('subjectService') private subjectService: () => SubjectService;

  public subjects: ISubject[] = [];

  @Inject('uploadService') private uploadService: () => UploadService;

  public uploads: IUpload[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.testingId) {
        vm.retrieveTesting(to.params.testingId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.testing.uploads = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.testing.id) {
      this.testingService()
        .update(this.testing)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.testing.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.testingService()
        .create(this.testing)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.testing.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveTesting(testingId): void {
    this.testingService()
      .find(testingId)
      .then(res => {
        this.testing = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.subjectService()
      .retrieve()
      .then(res => {
        this.subjects = res.data;
      });
    this.uploadService()
      .retrieve()
      .then(res => {
        this.uploads = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
