import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IUpload } from '@/shared/model/upload.model';
import UploadService from './upload.service';

@Component
export default class UploadDetails extends mixins(JhiDataUtils) {
  @Inject('uploadService') private uploadService: () => UploadService;
  public upload: IUpload = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.uploadId) {
        vm.retrieveUpload(to.params.uploadId);
      }
    });
  }

  public retrieveUpload(uploadId) {
    this.uploadService()
      .find(uploadId)
      .then(res => {
        this.upload = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
