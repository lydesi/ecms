import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import PostService from '@/entities/post/post.service';
import { IPost } from '@/shared/model/post.model';

import SubjectService from '@/entities/subject/subject.service';
import { ISubject } from '@/shared/model/subject.model';

import GroupService from '@/entities/group/group.service';
import { IGroup } from '@/shared/model/group.model';

import TestingService from '@/entities/testing/testing.service';
import { ITesting } from '@/shared/model/testing.model';

import { IUpload, Upload } from '@/shared/model/upload.model';
import UploadService from './upload.service';

const validations: any = {
  upload: {
    filename: {},
    data: {},
  },
};

@Component({
  validations,
})
export default class UploadUpdate extends mixins(JhiDataUtils) {
  @Inject('uploadService') private uploadService: () => UploadService;
  public upload: IUpload = new Upload();

  @Inject('postService') private postService: () => PostService;

  public posts: IPost[] = [];

  @Inject('subjectService') private subjectService: () => SubjectService;

  public subjects: ISubject[] = [];

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];

  @Inject('testingService') private testingService: () => TestingService;

  public testings: ITesting[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.uploadId) {
        vm.retrieveUpload(to.params.uploadId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.upload.id) {
      this.uploadService()
        .update(this.upload)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.upload.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.uploadService()
        .create(this.upload)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecmsApp.upload.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveUpload(uploadId): void {
    this.uploadService()
      .find(uploadId)
      .then(res => {
        this.upload = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.postService()
      .retrieve()
      .then(res => {
        this.posts = res.data;
      });
    this.subjectService()
      .retrieve()
      .then(res => {
        this.subjects = res.data;
      });
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
    this.testingService()
      .retrieve()
      .then(res => {
        this.testings = res.data;
      });
  }
}
