import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IUpload } from '@/shared/model/upload.model';

import JhiDataUtils from '@/shared/data/data-utils.service';

import UploadService from './upload.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Upload extends mixins(JhiDataUtils) {
  @Inject('uploadService') private uploadService: () => UploadService;
  private removeId: number = null;

  public uploads: IUpload[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllUploads();
  }

  public clear(): void {
    this.retrieveAllUploads();
  }

  public retrieveAllUploads(): void {
    this.isFetching = true;

    this.uploadService()
      .retrieve()
      .then(
        res => {
          this.uploads = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IUpload): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeUpload(): void {
    this.uploadService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecmsApp.upload.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllUploads();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
