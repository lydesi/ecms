import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Subject = () => import('@/entities/subject/subject.vue');
// prettier-ignore
const SubjectUpdate = () => import('@/entities/subject/subject-update.vue');
// prettier-ignore
const SubjectDetails = () => import('@/entities/subject/subject-details.vue');
// prettier-ignore
const Post = () => import('@/entities/post/post.vue');
// prettier-ignore
const PostUpdate = () => import('@/entities/post/post-update.vue');
// prettier-ignore
const PostDetails = () => import('@/entities/post/post-details.vue');
// prettier-ignore
const Testing = () => import('@/entities/testing/testing.vue');
// prettier-ignore
const TestingUpdate = () => import('@/entities/testing/testing-update.vue');
// prettier-ignore
const TestingDetails = () => import('@/entities/testing/testing-details.vue');
// prettier-ignore
const Upload = () => import('@/entities/upload/upload.vue');
// prettier-ignore
const UploadUpdate = () => import('@/entities/upload/upload-update.vue');
// prettier-ignore
const UploadDetails = () => import('@/entities/upload/upload-details.vue');
// prettier-ignore
const Group = () => import('@/entities/group/group.vue');
// prettier-ignore
const GroupUpdate = () => import('@/entities/group/group-update.vue');
// prettier-ignore
const GroupDetails = () => import('@/entities/group/group-details.vue');
// prettier-ignore
const Profile = () => import('@/entities/profile/profile.vue');
// prettier-ignore
const ProfileUpdate = () => import('@/entities/profile/profile-update.vue');
// prettier-ignore
const ProfileDetails = () => import('@/entities/profile/profile-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/subject',
    name: 'Subject',
    component: Subject,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/subject/new',
    name: 'SubjectCreate',
    component: SubjectUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/subject/:subjectId/edit',
    name: 'SubjectEdit',
    component: SubjectUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/subject/:subjectId/view',
    name: 'SubjectView',
    component: SubjectDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/post',
    name: 'Post',
    component: Post,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/post/new',
    name: 'PostCreate',
    component: PostUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/post/:postId/edit',
    name: 'PostEdit',
    component: PostUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/post/:postId/view',
    name: 'PostView',
    component: PostDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/testing',
    name: 'Testing',
    component: Testing,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/testing/new',
    name: 'TestingCreate',
    component: TestingUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/testing/:testingId/edit',
    name: 'TestingEdit',
    component: TestingUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/testing/:testingId/view',
    name: 'TestingView',
    component: TestingDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/upload',
    name: 'Upload',
    component: Upload,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/upload/new',
    name: 'UploadCreate',
    component: UploadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/upload/:uploadId/edit',
    name: 'UploadEdit',
    component: UploadUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/upload/:uploadId/view',
    name: 'UploadView',
    component: UploadDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group',
    name: 'Group',
    component: Group,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/new',
    name: 'GroupCreate',
    component: GroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/:groupId/edit',
    name: 'GroupEdit',
    component: GroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/:groupId/view',
    name: 'GroupView',
    component: GroupDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/profile/new',
    name: 'ProfileCreate',
    component: ProfileUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/profile/:profileId/edit',
    name: 'ProfileEdit',
    component: ProfileUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/profile/:profileId/view',
    name: 'ProfileView',
    component: ProfileDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
