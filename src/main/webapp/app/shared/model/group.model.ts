import { IUpload } from '@/shared/model/upload.model';
import { ISubject } from '@/shared/model/subject.model';

export interface IGroup {
  id?: number;
  name?: string | null;
  description?: string | null;
  logoContentType?: string | null;
  logo?: string | null;
  uploads?: IUpload[] | null;
  subjects?: ISubject[] | null;
}

export class Group implements IGroup {
  constructor(
    public id?: number,
    public name?: string | null,
    public description?: string | null,
    public logoContentType?: string | null,
    public logo?: string | null,
    public uploads?: IUpload[] | null,
    public subjects?: ISubject[] | null
  ) {}
}
