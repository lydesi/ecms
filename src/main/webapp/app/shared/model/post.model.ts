import { ISubject } from '@/shared/model/subject.model';
import { IUpload } from '@/shared/model/upload.model';

export interface IPost {
  id?: number;
  title?: string;
  description?: string | null;
  visible?: boolean | null;
  logoContentType?: string | null;
  logo?: string | null;
  subject?: ISubject | null;
  uploads?: IUpload[] | null;
}

export class Post implements IPost {
  constructor(
    public id?: number,
    public title?: string,
    public description?: string | null,
    public visible?: boolean | null,
    public logoContentType?: string | null,
    public logo?: string | null,
    public subject?: ISubject | null,
    public uploads?: IUpload[] | null
  ) {
    this.visible = this.visible ?? false;
  }
}
