import { IGroup } from '@/shared/model/group.model';
import { IUser } from '@/shared/model/user.model';

import { UserStatus } from '@/shared/model/enumerations/user-status.model';
export interface IProfile {
  id?: number;
  status?: UserStatus;
  avatarContentType?: string | null;
  avatar?: string | null;
  group?: IGroup | null;
  user?: IUser | null;
}

export class Profile implements IProfile {
  constructor(
    public id?: number,
    public status?: UserStatus,
    public avatarContentType?: string | null,
    public avatar?: string | null,
    public group?: IGroup | null,
    public user?: IUser | null
  ) {}
}
