import { IUpload } from '@/shared/model/upload.model';
import { IGroup } from '@/shared/model/group.model';

export interface ISubject {
  id?: number;
  name?: string;
  description?: string | null;
  logoContentType?: string | null;
  logo?: string | null;
  uploads?: IUpload[] | null;
  groups?: IGroup[] | null;
}

export class Subject implements ISubject {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string | null,
    public logoContentType?: string | null,
    public logo?: string | null,
    public uploads?: IUpload[] | null,
    public groups?: IGroup[] | null
  ) {}
}
