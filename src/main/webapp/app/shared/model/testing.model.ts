import { ISubject } from '@/shared/model/subject.model';
import { IUpload } from '@/shared/model/upload.model';

export interface ITesting {
  id?: number;
  title?: string;
  description?: string | null;
  url?: string;
  subject?: ISubject | null;
  uploads?: IUpload[] | null;
}

export class Testing implements ITesting {
  constructor(
    public id?: number,
    public title?: string,
    public description?: string | null,
    public url?: string,
    public subject?: ISubject | null,
    public uploads?: IUpload[] | null
  ) {}
}
