import { IPost } from '@/shared/model/post.model';
import { ISubject } from '@/shared/model/subject.model';
import { IGroup } from '@/shared/model/group.model';
import { ITesting } from '@/shared/model/testing.model';

export interface IUpload {
  id?: number;
  filename?: string | null;
  dataContentType?: string | null;
  data?: string | null;
  posts?: IPost[] | null;
  subjects?: ISubject[] | null;
  groups?: IGroup[] | null;
  testings?: ITesting[] | null;
}

export class Upload implements IUpload {
  constructor(
    public id?: number,
    public filename?: string | null,
    public dataContentType?: string | null,
    public data?: string | null,
    public posts?: IPost[] | null,
    public subjects?: ISubject[] | null,
    public groups?: IGroup[] | null,
    public testings?: ITesting[] | null
  ) {}
}
