import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class PostUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('ecmsApp.post.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  titleInput: ElementFinder = element(by.css('input#post-title'));

  descriptionInput: ElementFinder = element(by.css('input#post-description'));

  visibleInput: ElementFinder = element(by.css('input#post-visible'));

  logoInput: ElementFinder = element(by.css('input#file_logo'));

  subjectSelect = element(by.css('select#post-subject'));

  uploadsSelect = element(by.css('select#post-uploads'));
}
