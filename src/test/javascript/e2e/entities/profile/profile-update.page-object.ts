import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class ProfileUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('ecmsApp.profile.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  statusSelect = element(by.css('select#profile-status'));

  avatarInput: ElementFinder = element(by.css('input#file_avatar'));

  groupSelect = element(by.css('select#profile-group'));

  userSelect = element(by.css('select#profile-user'));
}
