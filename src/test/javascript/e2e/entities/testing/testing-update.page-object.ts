import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class TestingUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('ecmsApp.testing.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  titleInput: ElementFinder = element(by.css('input#testing-title'));

  descriptionInput: ElementFinder = element(by.css('input#testing-description'));

  urlInput: ElementFinder = element(by.css('input#testing-url'));

  subjectSelect = element(by.css('select#testing-subject'));

  uploadsSelect = element(by.css('select#testing-uploads'));
}
