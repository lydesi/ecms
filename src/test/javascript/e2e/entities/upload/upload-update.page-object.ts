import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class UploadUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('ecmsApp.upload.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  filenameInput: ElementFinder = element(by.css('input#upload-filename'));

  dataInput: ElementFinder = element(by.css('input#file_data'));
}
