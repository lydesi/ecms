/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import TestingDetailComponent from '@/entities/testing/testing-details.vue';
import TestingClass from '@/entities/testing/testing-details.component';
import TestingService from '@/entities/testing/testing.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Testing Management Detail Component', () => {
    let wrapper: Wrapper<TestingClass>;
    let comp: TestingClass;
    let testingServiceStub: SinonStubbedInstance<TestingService>;

    beforeEach(() => {
      testingServiceStub = sinon.createStubInstance<TestingService>(TestingService);

      wrapper = shallowMount<TestingClass>(TestingDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { testingService: () => testingServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundTesting = { id: 123 };
        testingServiceStub.find.resolves(foundTesting);

        // WHEN
        comp.retrieveTesting(123);
        await comp.$nextTick();

        // THEN
        expect(comp.testing).toBe(foundTesting);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTesting = { id: 123 };
        testingServiceStub.find.resolves(foundTesting);

        // WHEN
        comp.beforeRouteEnter({ params: { testingId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.testing).toBe(foundTesting);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
