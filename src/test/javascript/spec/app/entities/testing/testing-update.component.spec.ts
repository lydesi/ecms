/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import TestingUpdateComponent from '@/entities/testing/testing-update.vue';
import TestingClass from '@/entities/testing/testing-update.component';
import TestingService from '@/entities/testing/testing.service';

import SubjectService from '@/entities/subject/subject.service';

import UploadService from '@/entities/upload/upload.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Testing Management Update Component', () => {
    let wrapper: Wrapper<TestingClass>;
    let comp: TestingClass;
    let testingServiceStub: SinonStubbedInstance<TestingService>;

    beforeEach(() => {
      testingServiceStub = sinon.createStubInstance<TestingService>(TestingService);

      wrapper = shallowMount<TestingClass>(TestingUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          testingService: () => testingServiceStub,

          subjectService: () => new SubjectService(),

          uploadService: () => new UploadService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.testing = entity;
        testingServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(testingServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.testing = entity;
        testingServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(testingServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTesting = { id: 123 };
        testingServiceStub.find.resolves(foundTesting);
        testingServiceStub.retrieve.resolves([foundTesting]);

        // WHEN
        comp.beforeRouteEnter({ params: { testingId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.testing).toBe(foundTesting);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
