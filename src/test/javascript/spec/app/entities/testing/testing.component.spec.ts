/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import TestingComponent from '@/entities/testing/testing.vue';
import TestingClass from '@/entities/testing/testing.component';
import TestingService from '@/entities/testing/testing.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Testing Management Component', () => {
    let wrapper: Wrapper<TestingClass>;
    let comp: TestingClass;
    let testingServiceStub: SinonStubbedInstance<TestingService>;

    beforeEach(() => {
      testingServiceStub = sinon.createStubInstance<TestingService>(TestingService);
      testingServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<TestingClass>(TestingComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          testingService: () => testingServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      testingServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllTestings();
      await comp.$nextTick();

      // THEN
      expect(testingServiceStub.retrieve.called).toBeTruthy();
      expect(comp.testings[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      testingServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeTesting();
      await comp.$nextTick();

      // THEN
      expect(testingServiceStub.delete.called).toBeTruthy();
      expect(testingServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
