/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import UploadUpdateComponent from '@/entities/upload/upload-update.vue';
import UploadClass from '@/entities/upload/upload-update.component';
import UploadService from '@/entities/upload/upload.service';

import PostService from '@/entities/post/post.service';

import SubjectService from '@/entities/subject/subject.service';

import GroupService from '@/entities/group/group.service';

import TestingService from '@/entities/testing/testing.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Upload Management Update Component', () => {
    let wrapper: Wrapper<UploadClass>;
    let comp: UploadClass;
    let uploadServiceStub: SinonStubbedInstance<UploadService>;

    beforeEach(() => {
      uploadServiceStub = sinon.createStubInstance<UploadService>(UploadService);

      wrapper = shallowMount<UploadClass>(UploadUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          uploadService: () => uploadServiceStub,

          postService: () => new PostService(),

          subjectService: () => new SubjectService(),

          groupService: () => new GroupService(),

          testingService: () => new TestingService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.upload = entity;
        uploadServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(uploadServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.upload = entity;
        uploadServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(uploadServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundUpload = { id: 123 };
        uploadServiceStub.find.resolves(foundUpload);
        uploadServiceStub.retrieve.resolves([foundUpload]);

        // WHEN
        comp.beforeRouteEnter({ params: { uploadId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.upload).toBe(foundUpload);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
