/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import UploadComponent from '@/entities/upload/upload.vue';
import UploadClass from '@/entities/upload/upload.component';
import UploadService from '@/entities/upload/upload.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Upload Management Component', () => {
    let wrapper: Wrapper<UploadClass>;
    let comp: UploadClass;
    let uploadServiceStub: SinonStubbedInstance<UploadService>;

    beforeEach(() => {
      uploadServiceStub = sinon.createStubInstance<UploadService>(UploadService);
      uploadServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<UploadClass>(UploadComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          uploadService: () => uploadServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      uploadServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllUploads();
      await comp.$nextTick();

      // THEN
      expect(uploadServiceStub.retrieve.called).toBeTruthy();
      expect(comp.uploads[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      uploadServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeUpload();
      await comp.$nextTick();

      // THEN
      expect(uploadServiceStub.delete.called).toBeTruthy();
      expect(uploadServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
